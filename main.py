#import random for the bot
from random import randint
# Varibles:
# Int playerWins
#   Counts up whenever the player wins a game
playerWins = 0
# Int computerWins
#   Counts up whenever the computer wins a game
computerWins = 0
# List results stores string type objects
#   contains the result of all interactions in the game, 
#   to add new items add all mesages to this list
#   Format should be Object Verb Object
results = [
    "Scissors cuts Paper",
    "Paper covers Rock",
    "Rock crushes Lizard",
    "Lizard poisons Spock",
    "Spock smashes Scissors",
    "Scissors decapitates Lizard",
    "Lizard eats Paper",
    "Paper disproves Spock",
    "Spock vaporizes Rock",
    "Rock crushes Scissors"
]
# LastResult stores a string of the last results
LastResult = ""

# Funtions:
#  Main:
def Main():
#   Import Global varibles
    global playerWins
    global computerWins
    global LastResult
#   Varibles:
#   String userInput
#       Used to store user input
    userInput = ""
#   string userItem
#       Stores an item that the user picked
    userItem = ""
#   string result
#       Stores the item the bot picked
    result = []
#   Method:
#   Call input() to get user input and puts output in userInput
    userInput = input("\nType your choice of item (q to exit): ")

    #check if user wants to quit
    if (userInput.lower() == "q"):
        exit

#   Call ParseInput() on userInput and store in userItem
    userItem = ParseInput(userInput)
    #if user inputed a bad item
    if (userItem == -1):
        
        return -1
    else:
    #   Call bot() on userItem and store output in result
        result = bot(userItem)
    #   set lastResult to the result
        LastResult = result
    #   case insenitive if position one of result is userItem
        if userItem == result.split(" ")[0]:
    #       increment playerwins
            playerWins += 1
    #   otherwise means the bot won
        else:
    #       increment computerWins
            computerWins += 1
#  ParseInput:
#   Takes userInput
def ParseInput(userInput):
#   Should return item
#   For loop over results:
    for result in results:
#       store result split by " " in resultSplit
        resultSplit = result.split(" ")
#       If userInput is the first value of resultSplit
        if userInput.lower() == resultSplit[0].lower():
#           return the first value of resultSplit
            return resultSplit[0]
#   return -1
    print ("wrong item mate")
    return -1
#  Bot:
#   Takes string userItem
def bot (userItem):
#   Varibles:
#       list posibleResults contains Strings
    possibleResults = []
#   Method:
#   for loop over results:
    for result in results:
#       list currResult is the split current result
        currResult = result.split(" ")
#       if currResult Contains userInput:
        if userItem in currResult:
#           Add result to posibleResults
            possibleResults.append(result)
#   Pick a random result from posibleResults and return it
    return possibleResults[randint (0,len(possibleResults)-1)]

#Infinite loop to count score
while True:
    #clear the screen for reprinting 
    print(chr(27)+'[2j')
    print('\033c')
    print('\x1bc')
    
    #print out all posible outcomes
    print ("posible outcomes: ", *results, sep='\n - ')

    #Inform user of their score
    print("You have won {} times. The computer has won {} times".format(playerWins, computerWins))

    #Print results from last round
    print ("\n{}".format(LastResult))

    #Execute main
    Main()